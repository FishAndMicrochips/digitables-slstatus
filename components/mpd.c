#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <regex.h>

#include <mpd/client.h>

#include "../util.h"

#define MPDHOST "localhost"
#define MPDPORT 6600

const char *
mpdStatusBar()
{
    struct mpd_connection *conn;
    struct mpd_status *status;
    struct mpd_song *song;
    enum mpd_state state;

    char song_state[32] = {'\0'};
    char song_title[32] = {'\0'};
    char song_artist[32] = {'\0'};

    size_t i;

    strcpy(song_state, "?");

    // CONNECTION MALLOC
    conn = mpd_connection_new(MPDHOST, MPDPORT, 1000);
    if(mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS){
        fprintf(stderr, "Could not connect to mpd: %s\n", mpd_connection_get_error_message(conn));
        goto mpd_connection_free;
    }

    // MALLOC STATUS
    if ((status = mpd_run_status(conn)) == NULL){
        fprintf(stderr, "Could not get mpd status: %s\n", mpd_status_get_error(status));
	    goto mpd_status_free;
    }


    state = mpd_status_get_state(status);
    if (state == MPD_STATE_PLAY) {
	    strcpy(song_state, "");
    } else if (state == MPD_STATE_PAUSE) {
	    strcpy(song_state, "");
    } else if (state == MPD_STATE_STOP) {
	    strcpy(song_state, "");
        goto mpd_status_free;
    } else {
	    goto mpd_status_free;
    }

    // MALLOC SONG
    if ((song = mpd_run_current_song(conn)) == NULL) {
        fprintf(stderr, "Could not get song name!\n");
	    goto mpd_song_free;
    }
    
    // Get song name
    strncpy(song_title, mpd_song_get_tag(song, MPD_TAG_TITLE, 0), 30);
    if (strlen(song_title) > 29) {
        // Add a ...
        for (i = strlen(song_title) - 3; i < strlen(song_title); ++i) {
            if (song_title[i] != '\0') {
                song_title[i] = '.';
            } 
        }
        song_title[31] = '\0';
    }

    // Get song artist
    strncpy(song_artist, mpd_song_get_tag(song, MPD_TAG_ARTIST, 0), 30);
    if (strlen(song_artist) > 29) {
        // Add a ...
        for (i = strlen(song_artist) - 3; i < strlen(song_artist); ++i) {
            if (song_artist[i] != '\0') {
                song_artist[i] = '.';
            } 
        }
        song_artist[31] = '\0';
    }

mpd_song_free:
    mpd_song_free(song);
mpd_status_free:
    mpd_status_free(status);
mpd_connection_free:
    mpd_connection_free(conn);
    return bprintf("(%s) %s - %s", song_state, song_artist, song_title);
}
